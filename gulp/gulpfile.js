const gulp = require("gulp");
const nodemon = require("gulp-nodemon");
const path = require("path");
const browserSync = require("browser-sync").create({});

const baseDir = path.join(__dirname, "..");
const buildFolder = path.join(baseDir, "public");

const assets = require("./assets");
assets.handleAssets(gulp, buildFolder, browserSync);

const css = require("./css");
css.handleCss(gulp, buildFolder, browserSync);

const ts = require("./typescript");
ts.handleTs(gulp, buildFolder, browserSync);

gulp.task("reload", function () {
    return browserSync.reload({ stream: true });
});

function initBrowser(done) {
    browserSync.init({
        proxy: "localhost:3005",
        port: 5000,
        files: ["../public"],
        notify: true,
        open: false,
    });

    done();
}

gulp.task("browser-sync", initBrowser);

gulp.task("server", function (cb) {
    var called = false;
    return nodemon({
        script: "../bin/www",
        ignore: ["gulp", "../node_modules"],
    }).on("start", function () {
        if (!called) {
            called = true;
            cb();
        }
    });
});

gulp.task("dev", gulp.series("scripts", "img", "styles", "other"));

gulp.task("watch", function () {
    gulp.watch("../src/css/**/*.scss", gulp.series("styles"));
    gulp.watch("../src/js/**/*.ts", gulp.series("scripts"));
    gulp.watch("../src/images/**/*.{png,jpe?g,gif}", gulp.series("img"));
    gulp.watch("../src/fonts/**/*.woff2", gulp.series("other"));
    gulp.watch("../views/**/*.hbs", gulp.series(browserSync.stream));
});

gulp.task(
    "default",
    gulp.series("dev")
);
