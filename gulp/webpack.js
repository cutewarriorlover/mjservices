const path = require("path");
const webpack = require("webpack");

module.exports.config = {
    mode: "development",
    entry: "./src/js/main.ts",
    context: path.resolve(__dirname, ".."),
    plugins: [
        new webpack.DefinePlugin({
            assert: "window.assert",
            assertAlways: "window.assert",
            abstract:
                "window.assert(false, 'abstract method called of: ' + (this.name || (this.constructor && this.constructor.name)));",
            G_HAVE_ASSERT: "true",
            G_IS_DEV: "true",
            G_BUILD_TIME: "" + new Date().getTime(),
        }),
        new webpack.HotModuleReplacementPlugin(),
    ],
    module: {
        rules: [
            { test: /\.(png|jpe?g|svg)$/, loader: "ignore-loader" },
            {
                test: /\.ts$/,
                enforce: "pre",
                exclude: /node_modules/,
                loader: "webpack-strip-block",
            },
            {
                test: /\.js$/,
                enforce: "pre",
                exclude: /node_modules/,
                use: [{ loader: "webpack-strip-block" }],
            },
        ],
    },
    resolve: {
        extensions: [".ts", ".js", ".json"],
    },
    output: {
        filename: "bundle.js",
        path: path.resolve(__dirname, "..", "build"),
    },
};
